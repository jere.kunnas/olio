package jere.kunnas.tehtava9;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AddUserActivity extends AppCompatActivity {

    private EditText inputFirstName;
    private EditText inputLastName;
    private EditText inputEmail;
    private String degree = "degree";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
    }
    User user = null;
    public void addUser(View view){
        inputFirstName = findViewById(R.id.txtFirstName);
        String firstName = inputFirstName.getText().toString();
        inputLastName = findViewById(R.id.txtLastName);
        String lastName = inputLastName.getText().toString();
        inputEmail = findViewById(R.id.txtEmail);
        String email = inputEmail.getText().toString();



        RadioGroup rgDegreeType = findViewById(R.id.rgDegreeType);

        switch(rgDegreeType.getCheckedRadioButtonId()){
            case R.id.rbtnTietotekniikka:
                degree = "Tietotekniikka";
                break;
            case R.id.rbtnTuotantotalous:
                degree = "Tuotantotalous";
                break;
            case R.id.rbtnLaskennallinen:
                degree = "Laskennallinen tekniikka";
                break;
            case R.id.rbtnSähkötekniikka:
                degree = "Sähkötekniikka";
                break;
        }
        //System.out.println(firstName + " " + lastName);
        System.out.println(email + " " + degree);

        user = new User(firstName, lastName, email, degree);
        UserStorage.getInstance().addUser(user);
        //System.out.println("Lisätty käyttäjä: " + user.getFirstName() + " " + user.getLastName() + ", " + user.getEmail());

    }
}