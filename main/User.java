package jere.kunnas.tehtava9;

import android.widget.RadioGroup;

public class User {
    private String firstName;
    private String lastName;
    private String email;
    private String degreeProgramme;
    private int image;

    public User(String firstName, String lastName, String email, String degreeProgramme){
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.degreeProgramme = degreeProgramme;





        switch(degreeProgramme){
            case "Tietotekniikka":
                image = R.drawable.tite;
                break;
            case "Tuotantotalous":
                image = R.drawable.tuta;
                break;
            case "Laskennallinen tekniikka":
                image = R.drawable.lake;
                break;
            case "Sähkötekniikka":
                image = R.drawable.sake;
                break;
        }
    }

    public int getImage() {
        return image;
    }

    public String getFirstName(){
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getDegreeProgramme() {
        return degreeProgramme;
    }
}
